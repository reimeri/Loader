package me;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class AssetLoader {
	
	private File directory = new File(System.getProperty("user.dir"));
	private File assets = new File(directory + "//assets");
	public static Asset asset = new Asset("false", Asset.AssetType.CHARACTER);
	public static ArrayList<Asset> assetList = new ArrayList();
	private String[] extensions = new String[] {"png", "jpg", "jpeg", "jfif", "jpe", "gif"};
	private ArrayList extensionsList = new ArrayList(Arrays.asList(extensions));
	
	public AssetLoader(){
		if(!assets.exists()){
			assets.mkdir();
		}
		loadAssets();
	}
	
	private void loadAssets(){
		BufferedReader breader = null;
		try{
			for(File file : assets.listFiles()){
				if(file.isDirectory()){
				asset = new Asset();
					for(File file1 : file.listFiles()){
						FileReader reader = new FileReader(file1);
						breader = new BufferedReader(reader);
						if(file1.getName().endsWith(".txt")){
							loadValues(breader);
						}else{
							loadPictures(file1);
						}
					}
				}
				assetList.add(asset);
			}
		}catch(Exception e){e.printStackTrace();}finally{
			if(breader != null){
				try {
					breader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void loadValues(BufferedReader breader) throws IOException{
		String text = breader.readLine();
		do{
			String[] list = text.split("=");
			if(list[0].equalsIgnoreCase("name"))
				asset.setName(list[1]);
			else if(list[0].equalsIgnoreCase("type"))
				asset.setTypeFromString(list[1]);
			text = breader.readLine();
		}while(text != null);
	}
	
	private void loadPictures(File file){
		for(String ex : extensions){
			if(file.getName().endsWith("." + ex)){
				String imgName = file.getName().split("\\.")[0];
				asset.getImages().put(imgName, file.getAbsolutePath());
			}
		}
	}

}
