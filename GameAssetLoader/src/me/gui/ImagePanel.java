package me.gui;

import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImagePanel extends Panel {
	
	public BufferedImage image;
	public int width, height;

	public ImagePanel(String imageName) {
		try {
			File input = new File(imageName);
			image = ImageIO.read(input);
			height = image.getHeight();
			width = image.getWidth();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	public void paint(Graphics g) {
		g.drawImage( image, 0, 0, null);
	}
	
}
