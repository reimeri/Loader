package me.gui;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import me.listener.GameMouseListener;

public class GameGui {
	
	public static JFrame frame;
	public ArrayList guiElements = new ArrayList();

	public GameGui(){
		drawGui();
	}
	
	public void drawGui(){
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {e.printStackTrace();}
		//JPanel panel = new JPanel();
		frame = new JFrame();
		frame.setSize(1000, 1000);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public ImagePanel drawButton(String buttonTexturePath, int x, int y){
		ImagePanel image = new ImagePanel(buttonTexturePath);
		image.setLayout(null);
		image.setBounds(x, y, image.width, image.height);
		frame.setLayout(null);
		frame.getContentPane().add(image);
		/*image.addMouseListener(new GameMouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.out.println(image.height);
			}
		});*/
		guiElements.add(image);
		return image;
	}
	
	public GuiText drawString(String text, int x, int y){
		GuiText guiText = new GuiText(text, x, y);
		//guiText.setBounds(x, y, text.length() * 200, 40);
		//guiText.setSize(100, 100);
		frame.setLayout(null);
		frame.getContentPane().add(guiText);
		guiElements.add(guiText);
		return guiText;
	}
	
	public void start(){
		OptionsGUI main = new OptionsGUI();
	}
	
}
