package me;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import lombok.Getter;
import me.gui.GameGui;
import me.gui.ImagePanel;
import me.listener.GameMouseListener;

public class AssetLoaderTester {
	
	private static AssetLoaderTester instance;
	private static GameGui gui = new GameGui();;

	public AssetLoaderTester(){ 
		this.instance = this;
	}

	public static void main(String[] args){
		AssetLoaderTester tester = new AssetLoaderTester();
		AssetLoader loader = new AssetLoader();
		gui.start();
		/*for(Asset asset : AssetLoader.assetList){
			System.out.println(asset.getName());
			if(asset.getType() != null)
				System.out.println(asset.getType());
			System.out.println(asset.getImages().values());
			System.out.println(asset.getImages().keySet());
			if(!asset.getImages().isEmpty()){
				for(String pathName : asset.getImages().values()){
					JFrame frame = new JFrame(asset.getName() + pathName);
					ImagePanel panel = new ImagePanel(pathName);
					frame.getContentPane().add(panel);
					int[] dimensions = new int[] {(int) (panel.width * 1.1), (int) (panel.height * 1.1)};
					frame.setSize(dimensions[0], dimensions[1]);
					frame.setSize(1000,1000);
					frame.setVisible(true);
				}
			}
		}*/
	}
	
	public static AssetLoaderTester getInstance(){
		return instance;
	}
	
	public GameGui getGui() {
		return gui;
	}

	public void setGui(GameGui gui) {
		this.gui = gui;
	}
}
