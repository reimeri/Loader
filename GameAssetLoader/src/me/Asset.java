package me;

import java.awt.image.BufferedImage;
import java.util.HashMap;

public class Asset {
	
	private String name;
	private AssetType type;
	private String description;
	private HashMap<String, String> images = new HashMap<String, String>();
	private HashMap values = new HashMap();
	
	public Asset(){}
	
	public Asset(String name, AssetType type){
		this.name = name;
		this.type = type.getType();
	}

	public AssetType getType() {
		return type;
	}

	public void setType(AssetType type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setTypeFromString(String type) {
		if(type.equalsIgnoreCase("CHARACTER"))
			this.type = AssetType.CHARACTER;
		if(type.equalsIgnoreCase("LOCATION"))
			this.type = AssetType.LOCATION;
		if(type.equalsIgnoreCase("EVENT"))
			this.type = AssetType.EVENT;
		if(type.equalsIgnoreCase("ITEM"))
			this.type = AssetType.ITEM;
	}
	
	public String toString(){
		return name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap<String, String> getImages() {
		return images;
	}

	public void setImages(HashMap<String, String> images) {
		this.images = images;
	}

	public HashMap getValues() {
		return values;
	}

	public void setValues(HashMap values) {
		this.values = values;
	}

	public enum AssetType {
		CHARACTER, LOCATION, EVENT,  ITEM;
		private final AssetType assetType;
		
		private AssetType(){
			assetType = this;
		}
		
		private AssetType getType(){
			return assetType;
		}
	
	}

}
